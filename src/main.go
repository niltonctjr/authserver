package main

import (
	"os"

	"github.com/labstack/echo/v4"
	"gitlab.com/niltonctjr/authserver/config"
	"gitlab.com/niltonctjr/authserver/middlewares"
	"gitlab.com/niltonctjr/authserver/routers"
	"gitlab.com/niltonctjr/authserver/utils"
)

func main() {
	if err := config.Init(); err != nil {
		utils.LogDanger(err)
	}

	// create instance
	e := echo.New()

	// middles
	if err := middlewares.Init(e); err != nil {
		utils.LogDanger(err)
	}

	//routes
	if err := routers.Init(e); err != nil {
		utils.LogDanger(err)
	}

	//start server
	e.Logger.Fatal(e.Start(os.Getenv("PORT_SERVER")))
}
