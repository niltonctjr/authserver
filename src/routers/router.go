package routers

import (
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/niltonctjr/authserver/models"
	"gitlab.com/niltonctjr/authserver/utils"
)

func Init(e *echo.Echo) error {

	configJWT := middleware.JWT([]byte(os.Getenv("JWT_SECRET")))

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK,
			map[string]interface{}{
				"message": "AuthServer Online",
			},
		)
	})
	e.POST("/register", func(c echo.Context) error {
		return c.String(http.StatusOK, "register")
	})
	e.POST("/login", func(c echo.Context) error {
		username := c.FormValue("username")
		password := c.FormValue("password")

		// buscar usuario na base de dados
		user := models.User{
			Name: username,
			Hash: utils.StrValueToPointer("$2a$14$OPTcJSNdczDkbc2PbCQLh.T.wXBMu6qo/pqhm9bI2jEx9YoyllFnO"), //123
		}
		if !user.CheckPasswordHash(password) {
			return echo.ErrUnauthorized
		}

		token := jwt.New(jwt.SigningMethodHS256)

		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["name"] = user.Name
		claims["admin"] = true
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, map[string]interface{}{
			"token": t,
		})
	})

	restrict := e.Group("/restrict")
	restrict.Use(configJWT)
	restrict.GET("", func(c echo.Context) error {
		return c.JSON(http.StatusOK,
			map[string]interface{}{
				"message": "Restrict area",
			},
		)
	})
	restrict.POST("/renovate", func(c echo.Context) error {
		return c.String(http.StatusOK, "renovate")
	})
	restrict.POST("/validate", func(c echo.Context) error {
		return c.String(http.StatusOK, "validate")
	})

	return nil
}
