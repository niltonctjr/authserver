package utils

import "github.com/fatih/color"

func LogWarning(err error) {
	c := color.New(color.FgYellow)
	c.Println(err.Error())
}

func LogDanger(err error) {
	c := color.New(color.FgHiRed)
	c.Println(err.Error())
}

func LogInfo(err error) {
	c := color.New(color.FgHiCyan)
	c.Println(err.Error())
}
