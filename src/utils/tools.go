package utils

func StrPointerToValue(pointer *string) string {

	return *pointer
}

func StrValueToPointer(pointer string) *string {

	return &pointer
}
