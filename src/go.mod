module gitlab.com/niltonctjr/authserver

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.10.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.2
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf
)
