package config

import (
	"os"
)

var CONFIGURATION map[string]string = map[string]string{
	"APP_ENV":              "development",
	"PORT_SERVER":          ":8080",
	"JWT_SECRET":           "secret",
	"JWT_TOKEN_EXPIRATION": "730",
}

func Init() error {
	for key, _ := range CONFIGURATION {
		if os.Getenv(key) == "" {
			os.Setenv(key, CONFIGURATION[key])
		}
	}
	return nil
}
