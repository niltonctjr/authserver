package middlewares

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var Reset = "\033[0m"
var Red = "\033[31m"
var Green = "\033[32m"
var Yellow = "\033[33m"
var Blue = "\033[34m"
var Purple = "\033[35m"
var Cyan = "\033[36m"
var Gray = "\033[37m"
var White = "\033[97m"

func Init(e *echo.Echo) error {

	e.Use(middleware.RecoverWithConfig(middleware.RecoverConfig{}))
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: Purple + "[${time_rfc3339_nano}]" +
			Green + " ${status} " +
			Cyan + "${method} ${path} " +
			Yellow + "(${remote_ip}) " +
			Red + "${latency_human}\n" + Reset,
	}))

	return nil
}
