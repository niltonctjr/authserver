package models

import (
	"gitlab.com/niltonctjr/authserver/utils"
	"golang.org/x/crypto/bcrypt"
)

type Model struct {
	Id *uint `json:id`
}

type User struct {
	Model
	Name     string  `json:"name"`
	Password *string `json:"password"`
	Hash     *string `json:"-"`
}

func (u User) HashPassword() error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(*u.Password), 14)
	if err != nil {
		return err
	}
	u.Hash = utils.StrValueToPointer(string(bytes))
	return nil
}

func (u User) CheckPasswordHash(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(*u.Hash), []byte(password))
	return err == nil
}
