## Auth Server
Auth Server é um micro serviço de autenticação para futuros projetos.
O projeto tem como base [GO](https://golang.org/) e o framework [echo.labstack](https://echo.labstack.com/).
Conforme o projeto evolui esta documentação ira se adequando.
A aplicação deve realizar as seguintes funcionalidades:

 - [ ] Registrar um usuario a ser autenticado;
 - [ ]  Gerar o Token de acesso;
 - [ ]  Validar Token;

### Requisitos
Instalar os softwares abaixo:

 - [GO](https://golang.org/doc/install)

### Para executar
Por hora o projeto esta em andamento e não tem ainda um serie de estrutura de build, por hora deve ser realizada a instalação do requisitos e executar os comandos abaixo:

    cd ./src
    go run main.go
    
### Testes
Ainda em desenvolvimento
